# Available Beds Platform

The available beds platform is designed to provide directory information with current data to stakeholders in recovery residences and allow recovery residence operators to easily make this data available.

A set of JavaScript modules have been developed for integration with the [Wix VELO](https://www.wix.com/velo/reference/api-overview) platfform to support this functionality with minimal adminsitrative overhead.  Backend code can be placed in the backend directory of a Wix Website.  PageCode can be placed on relevant pages, changing the ## values to address the relevant page items.

This project was designed by and developed for the New Hampshire Coalition of Recovery Residences ([production version](https://www.nhcorr.org/available-residences)). This project was made possible by the generous support of the TD Charitable Foundation and AmeriHealth Caritas New Hampshire.

## Directory

Any residences that are displayed in this table have current certification as of the date when the page was rendered.  Any residences with invalid certifications are automatically removed.

The directory provides the following data for each certified residence.

 - The name of the residence with a clickable link to its operator's website or Facebook page.
 - The town or city where the residence is located.
 - Any demographic restrictions for the residence.
 - The certification level of the residence.
 - The certified capacity of the residence.
 - The date when the residence first earned their certification.

``` js
loadHouses().then(res => {
		$w('#table2').rows = res.items.map(item =>	{
			return{
				//add link
				residence_name: item.title,
				residence_link: item.url,
				municipality: item.houseAddress.city,
				certification_level: item.certificationLevel,
				certified_since: item.certifiedSince,
				maximum_capacity: item.totalBeds,
				demographics: item.demographic,
			};
		})
	});
```

### Aggregate by Demographic Restrictions
This table shows the state-wide total number of residences and state-wide total capacity for each demographic.

``` js
	loadAggregiateDemographic().then(res => {
		$w('#table3').rows = res._items.map(item =>	{
				return{
					demographic: item._id,
					number_of_residences: item.count,
					total_capacity: item.totalBedsSum
				};
			});
	});
```

### Aggregate by Certification Level
This table shows the state-wide total number of residences and state-wide total capacity for each certification level.

``` js
	loadAggregiateCertificationLevel().then(res => {
		$w('#table1').rows = res._items.map(item =>	{
			return{
				certification_level: item._id,
				number_of_residences: item.count,
				total_capacity: item.totalBedsSum
			};
		});
	});
```
### Geographic Directory

The geographic directory can be used to browse certified residences, plotted near their physical locations across the state of New Hampshire.
The map displays unique pin colors for each demographic.
The maps features (e.g. zoom, geolocation data) are restricted to protect the privacy of the residences.

Pins are collapsed for multiple houses within the same town or city that share the same operator.
Each map pin displays the name of the resdience or group of resdiences, a link to the operators' website or Facebook page, and any applicable demographic restrictions.
If a pin represents more than one residence, the number of residences that it represents is displayed.
``` js
	generateOsmMarkers().then(oMarkers => {
		$w("#html1").postMessage(oMarkers);
    //uses these markers: https://github.com/pointhi/leaflet-color-markers
	});
```

## Certified Residences Accepting Applications

This page provides contact data for operators who are advertising certified recovery residence availability through the available beds platform.
All data on this page is no more than 14 days old, and will be automatically removed after that point. 
Only currently certified residences are shown.

### Directory of Residences Accepting Applications

The directory table provides information for operators of recovery residences who are advertising availability. 

Each row in the directory table may represent multiple residences that share all of the following:

 - the same operator
 - the same town or city
 - the same demographic restrictions

Any residences that are displayed in this table have current certification as of the date when the page was rendered.  Any residences with invalid certifications are automatically removed.

The directory provides the following data for each certified residence.

 - The name of the residence with a clickable link to its operator's website or Facebook page.
 - The town or city where the residence is located.
 - Any demographic restrictions for the residence.
 - The certification level of the residence.
 - The most recent date when the residences' availability was updated.

 ``` js
 loadHouses().then(res => {
		$w('#table2').rows = res._items.map(item =>	{
			const splitString = item.houseGroup.split(',');
			const name = splitString[0];
			const municipality = splitString[1];
			const certification = splitString[3];
			const demographic = splitString[2];
			const url = splitString[4];

			return{
				residence_name: name,
				residence_link: url,
				municipality: municipality,
				certification_level: certification,
				updated_date: item._updatedDateMin,
				demographics: demographic,
			};
		});
	});
 ```


### Aggregate by Demographic Restrictions
This table shows the state-wide total number of residences that are advertising availability for new residents for each demographic.

``` js
	loadAggregiateDemographic().then(res => {
		$w('#table3').rows = res._items.map(item =>	{
			return{
				demographic: item._id,
				number_of_residences: item.count
			};
		});
	});
```

### Aggregate by Certification Level
This table shows the state-wide total number of residences that are advertising availability for new residents for each certification level.

``` js
	loadAggregiateCertificationLevel().then(res =>{
		$w('#table1').rows = res._items.map(item =>	{
			return{
				certification_level: item._id,
				number_of_residences: item.count
				//available_capacity: (item.totalBedsSum - item.filledBedsSum)
			};
		});
	});
```

### Geographic Directory of Residences Accepting Applications

The geographic directory can be used to browse certified residences, plotted near their physical locations across the state of New Hampshire.
The map displays unique pin colors for each demographic.
The maps features (e.g. zoom, geolocation data) are restricted to protect the privacy of the residences.

Pins are collapsed for multiple houses within the same town or city that share the same operator.
Each map pin displays the name of the resdience or group of resdiences, a link to the operators' website or Facebook page, and any applicable demographic restrictions.
While a pin might represent multiple residences with availability, the number of residences is not shown.

The map is implemented through Leaflet to support both marker pins and OpenStreetMap tiles from MapBox.

``` js
	generateOsmMarkers().then(oMarkers => {
		$w("#html1").postMessage(oMarkers);
	})
```


## For Owners/Operators: Enter Filled Beds

This page is a data-entry portal for owners and operators of recovery residences. Only one account may be assigned to manage a residence. A Google account may be used to authenticate a manager.

``` js
async function loadData(){
	$w('#button2').onClick(async (event) => {
		const $item = $w.at(event.context);
		const itemID = event.context.itemId;
		const data = $w("#repeater4").data;
		const itemData = data.find((item) => item._id === itemID);
		
  		if($item('#input12').valid && (parseInt($item('#input12').value) >= 0)){
			$item("#text119").text = "Your content has been submitted!";

			//save advertiseAvailability and Filled Beds to database
			itemData.filledBeds = parseInt($item('#input12').value);
			if (itemData.filledBeds > itemData.totalBeds){
				Math.min(itemData.filledBeds,itemData.totalBeds);
			}
			itemData.advertiseAvailability = $item('#switch1').checked;
			saveData(itemData).catch((err) => {
				$item('#text119').text = "Server Error! Check Console";
			});

		}else{
			$item("#text119").text = "Please enter a valid whole number.";
			$item('#input12').value = '';
		}
		$item("#button2").disable();
		$item("#text119").show();
	});


	loadHouses(wixUsers.currentUser).then(items => {
		$w('#repeater4').data = items;
	});

	$w('#repeater4').onItemReady(($item, itemdata, index) => {
		$item("#text119").hide();
		$item("#button2").hide();
		$item("#text112").text = itemdata.title;
		$item("#text111").text = itemdata.houseAddress.formatted;
		$item("#text110").text = "Total Capacity: " + itemdata.totalBeds;
		$item("#switch1").checked = itemdata.advertiseAvailability;
	});
}
```

### What data is displayed?

This form will show current data from the database about each residence.
    - The title of the residence. This is the name that displays in the residence directory.
    - The total capacity of the residence. This is displayed in the residence directory.
    - The street address of the residence. This is not displayed in the residence directory, and is only accessible by staff and the associated manager.
    - An on-off switch to enable and disable advertising available beds at this residence.  A blue switch is ON.

The form will only allow you to change two attributes of the property: you may enter a number of beds, and you may turn the advertise availability function on and off.

After entering a number in the *filled beds* box, a *save* button will appear.  Pressing the *save* button will save the value of *filled beds* and *advertise availability* switch into the database and instantly update all other portions of the website. 

You always must enter a number in the *filled beds* box before pressing save.  To turn the *advertise availability* feature on or off, you must first update the *filled beds* value, check the value of the *advertise* switch, and then press save.

A confirmation message will be displayed if your data has been properly updated in the database.  If you receive a message of *server error* please wait 15 minutes and try again. If the issue persists, contact Staff.

## For Owners/Operators: View Your Data

In this view, you *cannot* change your data. You may compare this data with your records and reconcile any discrepancies with the Staff.

### Residence Directory

This table shows the data stored about the residences you manage. This view is read-only, 

 -  The title of the residence. This is displayed in the public residence directories.
 -  A link to the operator's website. This is displayed in the public residence directories.
 -  The address of the residence. Only the city and state are shown properly, otherwise this information can only be viewed by the relevant manager and the staff.
 -  The demographic restrictions for the residence. Multiple tags may be displayed, as appropriate.
 -  The certification level of the residence. 
 -  The current occupancy of the residence. This is used to calculate availability (as  a yes/no value) and is only available to the relevant manager and Staff.
 -  The first date of certification for the residence.
 -  The certification expiration date. Only currently certified residences are shown in this table.
 -  The last date that the data (occupancy and advertise availability) were updated.

``` js
loadHouses(user).then(res => {
			$w('#table2').rows = res.map(item =>	{
				return{
					//add link
					residence_name: item.title,
					residence_link: item.url,
					municipality: item.houseAddress.formatted,
					certification_level: item.certificationLevel,
					certified_since: item.certifiedSince,
					certified_until: item.certificationExpiration,
					occupancy: item.filledBeds,
					advertise: item.advertiseAvailability,
					demographics: item.demographic,
					updated_date: item._updatedDate
				};
			});
```

### Geographic Directory
This map shows each residence, plotted as they will be in the directory of certified residences.  Map pins include clickable links and demographic information.

``` js
	generateOsmMarkers(user).then(oMarkers => {
			$w("#html1").postMessage(oMarkers);
		//uses these markers: https://github.com/pointhi/leaflet-color-markers
		});
```


### Aggregate Tables
This table shows summary statistic for all the residences that the manager has access to.

``` js
	loadAggregiateCertificationLevel(user).then(res => {
			$w('#table4').rows = res._items.map(item =>	{
				return{
					certification_level: item._id,
					number_of_residences: item.count,
					total_capacity: item.totalBedsSum
				};
			});
		});
    loadAggregiateDemographic(user).then(res => {
			$w('#table5').rows = res._items.map(item =>	{
					return{
						demographic: item._id,
						number_of_residences: item.count,
						total_capacity: item.totalBedsSum
					};
				});
		});
```

## For Staff: Automated Emails
Reminder emails will be sent (at most one per day) to residence managers, when their residence's data is between 7 and 28 days old. Reminder emails are triggered when data is changed in the database, so while they will run at most one time per day, they may not run at the same time every day.

Remainder Email templates can be accessed in Wix through the Marketing and SEO panel in the Triggered Emails menu.  Email tempaltes may be edited as long as they keep their Triggered Email ID.

### Weekly Data Update Reminders
When data is 7 to 13 days old, there is a very friendly reminder that the record is active in the system but the data should be updated to stay current.

### Weekly Out-Of-Date Data Reminders
When data is 14 to 28 days old, there is a reminder that the data is out of date and not showing on public systems. This email includes a reminder that their access may be suspended. **This message is also sent on the 14th day (when the data expires).**

After 28 days, the system will stop sending reminders. After 42 days, the record will be automatically suspended. To make a suspended record active again, simply change any property in the *Certified Residences* Data Table in the Content Manager for that house.  That will change the date last updated in the system, and make the record live in both the member portal and the available beds page.

Suggestion: to make a record live, set the advertise availability switch to off. This will allow the owner/operator to have access to the record in their portal while retaining the control for them to switch the record to 'on'.


## For Staff: Content Manager

The content manager gives access to the   *CertifiedResidences* database.

Databases in Wix have two mode: Sandbox and Live.  The sandbox mode is used to test features in development. Anytime the *preview page* mode is used in the editor, the sandbox mode of the database is used. This should only be used for web development and testing. Changes will not affect the live version without using the `sync feature'.

Changes to the Live database are *immediately* reflected on the website.  There is no need to publish new pages to show new database data on the webpage. All database-linked content is dynamically generated with *javascript* and is reprocessed for each viewers' individual web request.

If you have made changes to the database and they are not reflected in your view of the website, your browser may have locally cached the page to save on data usage. Clearing the cache of your browser is usually effective to ensure that refreshing views the most recent version.

## Known Quirks of Wix Database

### Addresses:
The address field does not always import strings correctly.  To fix this, delete the address for each entry and retype it to the from.  A map search will open, select the matching address. This will import all relevant data from Google Maps.

### Website URLs:
The Website URL field must not be left blank, and *must use https://*. Empty urls may cause the map to fail to render its pins.

### JSON Array:
The utilization history field embeds (effectively) a spreadsheet within a single cell of the data table.
This array gives a high temporal resolution for each residence, provide a detailed record - but it requires some additional work to get the extra dimensions in the table.
This is designed to be expanded during analysis in 'R' or similar languages, but can be quickly expanded with web based tools for item-by-item analysis. For example, https://csvjson.com/json2csv.

## For Staff: Database Design

### Certified Residences (Data Table)

The Certified Residences data table is the primary record of all data about certified houses.  It includes many fields, some editable by staff and some calculated.  

The following fields are editable by staff:

 - House Address (JSON) the geolocation data for the residence. This is used to place map pins and add municipiality values
 - Advertise Availability (bool): whether or not the residence should display in the \emph{Available Residences} page
 - MAT/MAR: custom tags for MAT/MAR restrictions (currently private)
 - ADA Accessibility: whether the residence meets accessibility requirements (currently private)
 - Certification Level: a tag for the certification level
 - Notes: a rich text box for any additional notes about a residence (currently private)
 - Certified Since: the initial date of certification for the residence
 - Certification Expiration: the date the certification expires.  Past-expiration residences will not display on the website, but will persist in the database
 - Total Beds: the certified capacity of the residence
 - Filled Beds: the number of beds currently occupied, regardless of advertisement of availability.
 - Organization: a tag for houses (typically with the same operator) that should appear clustered on the availability page
 - Website: the url for the website.  MUST be HTTPS://***, cannot be null
 - Manager: the owner/operator account assigned to manage the residence.  Can only be assigned after a valid account has been requested and confirmed.
 - Demographic: the demographic restrictions of the residence.

The following fields are also editable by owners/operators:

 - Advertise Availability: whether or not the residence should display in the \emph{Available Residences} page
 - Filled Beds: the number of filled Beds

The following fields are computed and should not be edited:
 - ID [SYSTEM]: a hexadecimal identifier for the row of the database, used to link to other tables
 - Owner [SYSTEM]: the Wix user that created the item
 - Created Date [SYSTEM]: the date and time the item was first added to the data table
 - Updated Date [SYSTEM]: the date and time the item was most recently modified by any user
 - Available Beds: the difference between total beds and filled beds.
 - Lat: latitude calculated from geolocation data for map pin
 - Long: longitude calculated from geolocation data for map pin
 - House Group: a tag that combines the organization, certification level and demographic restrictions for quick sorting and aggregate functions.
 - City/Town: the municipality data from the geolocation (for aggregate functions only).  Null if geolocation data is a string and not a JSON.
 - Utilization History: a JSON array (for detailed analysis in R, pyhton or MatLab), residence-by-residence detailed history containing the same information as the data log.


### Data Log (Data Table)
The data log will get a new entry any time data is changed in the data table by any user. This data table is designed to highlight the most commonly changed fields at a high temporal resolution, with minimal need to anonymous data for external analysis.

The House Address and Title are identifying information.

 - Title: the name of the house
 - Created Date: the data when the data was updated
 - House Address: the address to identify the house when names are ambiguous, as a human-readable string
 - House ID: the ID of the house in the Certified Residences Database (only editable by Staff)
 - Demographic: the demographic restrictions of the house (only editable by Staff)
 - Certification Level: the certification level of the house (only editable by Staff)
 - Total Beds: the total number of beds the house is certified for (only editable by Staff)
 - Filled Beds: the number of beds filled (editable by the Owner/Operator)
 - Available Beds: calculated difference between filled beds and total beds
 - Percent Utilized: the percentage of capacity that has been filled (i.e. filled/total).

This data table should only be downloaded for reports, and should not be modified within the content manager.

### Aggregate Data Log (Data Table)
The aggregate data log will get a new set of entries each day, if the data in the Certified Residence Database has change.  This is triggered by the first change in data for a day. This data table has eight fields: four three identifying fields and five computed statistic fields. These fields are assigned in the \emph{data.js} module.

As aggregate statistics, this data is anonymous and may be shared without divulging identifying information about the houses.

 - Title: the name of the group that the static was run on.
 - Created Date: the date and time the statistic was run
 - Field: the field the grouping was applied to (e.g. certification level, demographic, no grouping)
 - Aggregate Count: The total number of filled beds within the grouping.
 - Aggregate Available: The total number of beds available within the grouping (as a sum of available beds). This includes unadvertised availability.
 - Aggregate capacity: the total number of filled and unfilled beds within the grouping.
 - Percent filled: the number of filled beds as a percentage of total capacity.
 - Number of residences: the number of residences included in this statistic (residences updated within 14 days that fit the grouping (e.g. demographic, certification level).

Each  statistic is only computed with data that is at most 14 days old. Older data is considered invalid and not considered as part of capacity or filled beds.  This will give a more accurate reporting of the percentage of filled beds as well as the available beds, but may give a conservative estimate of the total capacity and total number of beds filled.

## Code for OSM Maps:
The Open Street Maps views of the database use an embedded HTML page as a Wix Object with the following custom code.  It requires an API key from Mapbox.

``` html
<!DOCTYPE html>
<html>
<head>
	
	<title>Quick Start - Leaflet</title>

	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<link rel="shortcut icon" type="image/x-icon" href="docs/images/favicon.ico" />

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>


	
</head>
<body>



<div id="mapid" style="width: 550px; height: 620px;"></div>
<script>

   
	var mymap = L.map('mapid').setView([43.5, -71], 8);

	L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=API_KEY', {
		maxZoom: 12,
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: 'mapbox/streets-v11',
		tileSize: 512,
		zoomOffset: -1
	}).addTo(mymap);
  // When data received
  window.onmessage = (event) => { 
    if (event.data) { 
      const OMarkers = event.data; 
      let markerCollection = L.featureGroup();
      for (const OMarker in OMarkers) {
        const marker = L.marker(
          [OMarkers[OMarker]["marker_lat"],OMarkers[OMarker]["marker_long"]], {
            title: OMarkers[OMarker]["marker_title"],
          }
        ).addTo(mymap);
        const popupLink = [OMarkers[OMarker]["marker_link"]][0];
        const popupText = [OMarkers[OMarker]["marker_popup"]][0]; 
        const popupDescription = OMarkers[OMarker]["marker_description"];
        const popupString = '<a href="' + popupLink + '" target="_blank">' + popupText + '</a>' + '<p>' + popupDescription + '</p>';
        marker.bindPopup(popupString).openPopup();
        markerCollection.addLayer(marker);
      }
      mymap.fitBounds(markerCollection.getBounds());

    } 
  };
</script>
</body>
</html>

```
