# Import for Wix Database (only export new entries):Fields may include the following, in a CSV.
Importing from ODS, XLSX, XLSM, XLS,  and PDF not supported.

## Title
    - Description: House or Organization Name.  Primary ID, cannot be null.
    - Field: title
    - Format: string

## Website
    - Description: Link to website or facebook page for organization.  Must be https://*
    - Field: url
    - Format: string

## Organization
    - Description: Group of houses that are combined in aggregate data.  May or may not be the manager. Display name for aggregate results.  Cannot be null. A single house can constitute an organization.  Only one tag allowed.
    - Field: orginization
    - Format: TAG as string array
      examples: 
        - ["Starting Point"]
        - [“Live Free”]

## Demographic
    - Description: Restrictions on demographics (e.g. female-only, male-only, seniors-only).  Cannot be null.  Multiple tags allowed.
    - Field: demographic
    - Format: TAG as string array
      examples: 
        - ```  ["male-only"] ``` 
        - ```  [“female-ony”] ``` 
        - ```  ["no-restrictions"] ``` 
        - ``` ["male-only","seniors-only"] ``` 


## Total Beds
    - Description: total capacity of certified residence
    - Field: totalBeds
    - Format: INT

## ADA Accessibility
    - Description: Does this residence meet ADA accessibility requirements?
    - Field: adaAccessible
    - Format: bool

## Certification

### Certification Level
    - Description: The level of certification of the residence
    - Field: certificationLevel
    - Format: TAG as string array.  Only one tag allowed.
      examples: 
        - ["II"]
        - [“III”]
        - ["IV"]

### Certification Date
    - Description: Date of initial certification completion
    - Field: certifiedSince
    - Format: DateTime: YYYY-MM-DDTHH:MM:SS

### Certification Expiration
    - Description: Date when certification must be renewed or is expired
    - Field: certificationExpiration
    - Format: DateTime: YYYY-MM-DDTHH:MM:SS

## House Address
    - Description: Geolocation data for house.  Can only accept one street number.  Cannot be null, but can be manually corrected in database if provided as formatted address string only (e.g. “2 1/2 Beacon Street, Box A-3, Suite 163, Concord, NH, 03301, USA”).
    - Field: houseAddress
    - Format: JSON
```json
{
	"city":"MUNICIPALITY_NAME",
	"location":{
		"latitude":DECIMAL_LATITUDE,
		"longitude":DECIMAL_LONGITUDE
	},
	"streetAddress":{
		"name":"STREET_NAME",
		"number":"STREET_NUMBER"
	},
	"formatted":"STREET_NUMBER STREET_NAME, MUNICIPALITY_NAME, STATE_ABV ZIP_CODE, USA",
	"country":"US",
	"postalCode":"ZIP_CODE",
	"subdivision":"STATE_ABV"
}
```

### MUNICIPALITY_NAME
- string Town or City name
### DECIMAL_LATITUDE
- float decimal latitude (not DMS)
### DECIMAL_LONGITUDE
- float demcial longitude (not DMS)
### STREET_NAME
- string Street Name, include abv (e.g., st., ave., etc.)
### STREET_NUMBER
- int or string, Street number.  *No Ranges, or ampersands*
### ZIP_CODE
- INT 5 digit postal code
### STATE_ABV
- 2 char string (i.e. "NH")
