// API Reference: https://www.wix.com/corvid/reference
import{loadAggregiateCertificationLevel} from 'backend/availableBeds.jsw';
import{loadAggregiateDemographic} from 'backend/availableBeds.jsw';
import{loadHouses} from 'backend/availableBeds.jsw';
import {generateOsmMarkers} from 'backend/availableBeds.jsw';
import wixWindow from 'wix-window';

$w.onReady(function () {

	//#region hide on mobile
	if(wixWindow.formFactor === "Mobile"){
			$w('#html1').collapse();
			$w('#table1').collapse();
			$w('#table3').collapse();
	}
	//#endregion

	//#endregion
//#region setup link in table rows:
	$w('#table2').columns = [
  {
    "id": "column_kjj4fkx2",
    "label": "Residence",
    "dataPath": "residence_name",
    "type": "string",
    "width": 100,
    "visible": true,
	"linkPath": "residence_link"
  },
  {
    "id": "column_kjj4wcg7",
    "label": "Municipality",
    "dataPath": "municipality",
    "type": "string",
    "width": 100,
    "visible": true
  },
  {
    "id": "column_kjj40a5y",
    "label": "Demographics",
    "dataPath": "demographics",
    "type": "string",
    "width": 100,
    "visible": true
  },
  {
    "id": "column_kjj4c1ha",
    "label": "Certification Level",
    "dataPath": "certification_level",
    "type": "number",
    "width": 100,
    "visible": true
  },
  {
    "id": "column_kjj49k8v",
    "label": "As Of",
    "dataPath": "updated_date",
    "type": "date",
    "width": 100,
    "visible": true
  }];
//#endregion
	loadData();
});

async function loadData() {
	loadAggregiateCertificationLevel().then(res =>{
		$w('#table1').rows = res._items.map(item =>	{
			return{
				certification_level: item._id,
				number_of_residences: item.count
				//available_capacity: (item.totalBedsSum - item.filledBedsSum)
			};
		});
	});
	loadAggregiateDemographic().then(res => {
		$w('#table3').rows = res._items.map(item =>	{
			return{
				demographic: item._id,
				number_of_residences: item.count
			};
		});
	});
	loadHouses().then(res => {
		$w('#table2').rows = res._items.map(item =>	{
			const splitString = item.houseGroup.split(',');
			const name = splitString[0];
			const municipality = splitString[1];
			const certification = splitString[3];
			const demographic = splitString[2];
			const url = splitString[4];

			return{
				residence_name: name,
				residence_link: url,
				municipality: municipality,
				certification_level: certification,
				updated_date: item._updatedDateMin,
				demographics: demographic,
			};
		});
	});
	generateOsmMarkers().then(oMarkers => {
		$w("#html1").postMessage(oMarkers);
	})
}
