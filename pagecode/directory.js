// API Reference: https://www.wix.com/corvid/reference
import{loadAggregiateCertificationLevel} from 'backend/residenceDirectory.jsw';
import{loadAggregiateDemographic} from 'backend/residenceDirectory.jsw';
import{loadHouses} from 'backend/residenceDirectory.jsw';
import {generateOsmMarkers} from 'backend/residenceDirectory.jsw';
import wixWindow from 'wix-window';

$w.onReady(function () {

		//#region hide on mobile
		if(wixWindow.formFactor === "Mobile"){
			$w('#html1').collapse();
			$w('#table1').collapse();
			$w('#table3').collapse();
		}
	//#endregion
//#region tableLinks
	//setup link in table rows:
	$w('#table2').columns = [
  {
    "id": "column_kjj4fkx2",
    "label": "Residence (Clickable Link)",
    "dataPath": "residence_name",
    "type": "string",
    "width": 100,
    "visible": true,
	"linkPath": "residence_link"
  },
  {
    "id": "column_kjj4wcg7",
    "label": "Municipality",
    "dataPath": "municipality",
    "type": "string",
    "width": 100,
    "visible": true
  },
  {
    "id": "column_kjj40a5y",
    "label": "Demographics",
    "dataPath": "demographics",
    "type": "string",
    "width": 100,
    "visible": true
  },
  {
    "id": "column_kjj4c1ha",
    "label": "Certification Level",
    "dataPath": "certification_level",
    "type": "number",
    "width": 100,
    "visible": true
  },
  {
    "id": "column_kjj40j83",
    "label": "Maximum Capacity",
    "dataPath": "maximum_capacity",
    "type": "string",
    "width": 100,
    "visible": true
  },
  {
    "id": "column_kjj49k8v",
    "label": "Certified Since",
    "dataPath": "certified_since",
    "type": "date",
    "width": 100,
    "visible": true
  }];
//#endregion
	loadData();
});

async function loadData() {
	loadAggregiateCertificationLevel().then(res => {
		$w('#table1').rows = res._items.map(item =>	{
			return{
				certification_level: item._id,
				number_of_residences: item.count,
				total_capacity: item.totalBedsSum
			};
		});
	});
	loadAggregiateDemographic().then(res => {
		$w('#table3').rows = res._items.map(item =>	{
				return{
					demographic: item._id,
					number_of_residences: item.count,
					total_capacity: item.totalBedsSum
				};
			});
	});
	loadHouses().then(res => {
		$w('#table2').rows = res.items.map(item =>	{
			return{
				//add link
				residence_name: item.title,
				residence_link: item.url,
				municipality: item.houseAddress.city,
				certification_level: item.certificationLevel,
				certified_since: item.certifiedSince,
				maximum_capacity: item.totalBeds,
				demographics: item.demographic,
			};
		})
	});
	generateOsmMarkers().then(oMarkers => {
		$w("#html1").postMessage(oMarkers);
    //uses these markers: https://github.com/pointhi/leaflet-color-markers
	});
}
