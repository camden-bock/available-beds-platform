// API Reference: https://www.wix.com/corvid/reference
import wixUsers from 'wix-users';
import {loadHouses} from 'backend/userReports.jsw';
import {saveData} from 'backend/userReports.jsw';
import wixData from 'wix-data';
import wixWindow from 'wix-window';

const today = new Date();

$w.onReady(function (){
	//#region hide on mobile
		if(wixWindow.formFactor === "Mobile"){
			$w('#quickActionBar1').collapse();
		}
	//#endregion

	$w('#repeater4').hide();
	$w('#text120').show();

	loadData();
});

async function loadData(){

	//button interactions

	$w("#button2").hide();
	$w("#button2").disable();

	$w("#text119").hide();

	$w('#input12').onInput(async (event) => {
		const $item = $w.at(event.context);
		$item("#button2").show();
		$item("#text119").hide();

		$item("#button2").enable();
	});

	$w('#switch1').onChange(async (event) => {
		const $item = $w.at(event.context);
		$item("#text119").hide();
		$item("#button2").show();
	});

	$w('#button2').onClick(async (event) => {
		const $item = $w.at(event.context);
		const itemID = event.context.itemId;
		const data = $w("#repeater4").data;
		const itemData = data.find((item) => item._id === itemID);
		
  		if($item('#input12').valid && (parseInt($item('#input12').value) >= 0)){
			$item("#text119").text = "Your content has been submitted!";

			//save advertiseAvailability and Filled Beds to database
			itemData.filledBeds = parseInt($item('#input12').value);
			if (itemData.filledBeds > itemData.totalBeds){
				Math.min(itemData.filledBeds,itemData.totalBeds);
				//TODO send error message to admin@nhcorr.org
				
			}
			itemData.advertiseAvailability = $item('#switch1').checked;
			saveData(itemData).catch((err) => {
				$item('#text119').text = "Server Error! Check Console";
				console.log(err);
			});

		}else{
			$item("#text119").text = "Please enter a valid whole number.";
			$item('#input12').value = '';
		}
		$item("#button2").disable();
		$item("#text119").show();
	});


	loadHouses(wixUsers.currentUser).then(items => {
		$w('#repeater4').data = items;
	});

	$w('#repeater4').onItemReady(($item, itemdata, index) => {
		$item("#text119").hide();
		$item("#button2").hide();
		$item("#text112").text = itemdata.title;
		$item("#text111").text = itemdata.houseAddress.formatted;
		$item("#text110").text = "Total Capacity: " + itemdata.totalBeds;
		$item("#switch1").checked = itemdata.advertiseAvailability;
	});
	$w('#text120').hide();
	$w('#repeater4').show();
}
