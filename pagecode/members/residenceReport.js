// API Reference: https://www.wix.com/corvid/reference
import{loadAggregiateCertificationLevel} from 'backend/userReports.jsw';
import{loadAggregiateDemographic} from 'backend/userReports.jsw';
import{loadHouses} from 'backend/userReports.jsw';
import {generateOsmMarkers} from 'backend/userReports.jsw';
import wixUsers from 'wix-users';
import wixData from 'wix-data';
import wixWindow from 'wix-window';


$w.onReady(async function () {
	//#region hide on mobile
		if(wixWindow.formFactor === "Mobile"){
			$w('#html1').collapse();
			$w('#table4').collapse();
			$w('#table5').collapse();
			$w('#quickActionBar1').collapse();
		}
	//#endregion

	//#region tableLinks
		//setup link in table rows:
			$w('#table2').columns = [
	{
		"id": "column1",
		"label": "Residence (Clickable Link)",
		"dataPath": "residence_name",
		"type": "string",
		"width": 100,
		"visible": true,
		"linkPath": "residence_link"
	},
	{
		"id": "column2",
		"label": "Address",
		"dataPath": "municipality",
		"type": "string",
		"width": 100,
		"visible": true
	},
	{
		"id": "column3",
		"label": "Demographics",
		"dataPath": "demographics",
		"type": "string",
		"width": 100,
		"visible": true
	},
	{
		"id": "column4",
		"label": "Certification Level",
		"dataPath": "certification_level",
		"type": "number",
		"width": 100,
		"visible": true
	},
	{
		"id": "column5",
		"label": "Occupancy",
		"dataPath": "occupancy",
		"type": "string",
		"width": 30,
		"visible": true
	},
	{
		"id": "column6",
		"label": "Certified Since",
		"dataPath": "certified_since",
		"type": "date",
		"width": 100,
		"visible": true
	},
	{
		"id": "column7",
		"label": "Certified Until",
		"dataPath": "certified_until",
		"type": "date",
		"width": 100,
		"visible": true
	},
	{
		"id": "column8",
		"label": "Data Last Updated",
		"dataPath": "updated_date",
		"type": "date",
		"width": 100,
		"visible": true
	}
	];
	//#endregion
	loadData(wixUsers.currentUser);
});

async function loadData(user){
	loadAggregiateCertificationLevel(user).then(res => {
			$w('#table4').rows = res._items.map(item =>	{
				return{
					certification_level: item._id,
					number_of_residences: item.count,
					total_capacity: item.totalBedsSum
				};
			});
		});
	loadAggregiateDemographic(user).then(res => {
			$w('#table5').rows = res._items.map(item =>	{
					return{
						demographic: item._id,
						number_of_residences: item.count,
						total_capacity: item.totalBedsSum
					};
				});
		});
	loadHouses(user).then(res => {
			$w('#table2').rows = res.map(item =>	{
				return{
					//add link
					residence_name: item.title,
					residence_link: item.url,
					municipality: item.houseAddress.formatted,
					certification_level: item.certificationLevel,
					certified_since: item.certifiedSince,
					certified_until: item.certificationExpiration,
					occupancy: item.filledBeds,
					advertise: item.advertiseAvailability,
					demographics: item.demographic,
					updated_date: item._updatedDate
				};
			});
	generateOsmMarkers(user).then(oMarkers => {
			$w("#html1").postMessage(oMarkers);
		//uses these markers: https://github.com/pointhi/leaflet-color-markers
		});
	});
}
