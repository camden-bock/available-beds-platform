import wixData from 'wix-data';

export function CertifiedResidences_beforeUpdate(item, context) {
	let hookContext = context;  // see below

	item.availableBeds = item.totalBeds - item.filledBeds;
	if(item.houseAddress.hasOwnProperty('location')){
		item.lat = item.houseAddress.location.latitude;
		item.lng = item.houseAddress.location.longitude;
	}else{
		item.lat = 0;
		item.lng = 0;
	}

	if(item.houseAddress.hasOwnProperty('city')){
		item.houseGroup = item.orginization + ',' + item.houseAddress.city + ',' + item.demographic + ',' + item.certificationLevel + ',' + item.url;
		item.municipality = item.houseAddress.city;
		item.houseAddressString = item.houseAddress.formatted;
	}else{
		item.houseGroup = item.orginization + ',' + " -- " + ',' + item.demographic + ',' + item.certificationLevel + ',' + item.url;
	}

	var expiration = item.certificationExpiration;
	expiration.setHours(23);
	expiration.setMinutes(59);
	item.certificationExpiration = expiration;

	var utilizationHistory = item.utilizationHistory;
	if(item.utilizationHistory == null){
		utilizationHistory = {"history": []};
	}

	const utilizationJSON = {
		"title": JSON.stringify(item.title), 
		"filledBeds": JSON.stringify(item.filledBeds),
		"totalBeds": JSON.stringify(item.totalBeds),
		"availableBeds": JSON.stringify(item.availableBeds),
		"percentUtilized": JSON.stringify(item.filledBeds/item.totalBeds),
		"houseAddress": JSON.stringify(item.houseAddress.formatted),
		"demographic": JSON.stringify(item.demographic),
		"certificationLevel": JSON.stringify(item.certificationLevel),
		"date": JSON.stringify(new Date())
	};

	//TODO add condition that checks if different than last.
	utilizationHistory.history.push(utilizationJSON);
	item.utilizationHistory = utilizationHistory;
	
  	return item;
}

export function CertifiedResidences_afterUpdate(item,context){
	let options = {  "suppressAuth": true,  "suppressHooks": true};
	wixData.insert('DataLog', {
		title: item.title, 
		filledBeds: item.filledBeds,
		totalBeds: item.totalBeds,
		availableBeds: item.availableBeds,
		percentUtilized: (item.filledBeds/item.totalBeds),
		addressString: item.houseAddress.formatted,
		demographic: item.demographic,
		certificationLevel: item.certificationLevel
		}
	, options);
	return item;
}

export function CertifiedResidences_beforeInsert(item, context) {
	//TODO: write your code here...
}

export function CertifiedResidences_afterInsert(item, context) {
	//TODO: write your code here...
}

export function CertifiedResidences_beforeRemove(itemId, context) {
	//TODO: write your code here...
}

export function CertifiedResidences_afterRemove(item, context) {
	//TODO: write your code here...
}

export function CertifiedResidences_beforeGet(itemId, context) {
	//TODO: write your code here...
}

export function CertifiedResidences_afterGet(item, context) {
	//TODO: write your code here...
}

export function CertifiedResidences_beforeQuery(query, context) {
	//TODO: write your code here...
}

export function CertifiedResidences_afterQuery(item, context) {
	//TODO: write your code here...
}

export function CertifiedResidences_beforeCount(query, context) {
	//TODO: write your code here...
}

export function CertifiedResidences_afterCount(count, context) {
	//TODO: write your code here...
}

export function CertifiedResidences_onFailure(error, context) {
	//TODO: write your code here...
}